import { ExceptionsHandler, LoggerService, UnknownRoutesHandler } from '@felicienfouillet/dev-tools-back';
import { ExperiencesController } from 'controllers/experiences.controller';
import { ProjectsController } from 'controllers/projects.controller';
import { TrainingsController } from 'controllers/trainings.controller';
import cors from 'cors';
import 'dotenv/config';
import { environment } from 'environment/environment';
import express from 'express';
import helmet from 'helmet';
import { connect } from 'mongoose';
import morgan from 'morgan';

/**
 * App variables
 */
const app = express();
const Logger: LoggerService = LoggerService.Instance({ loggerName: 'Portfolio API', saveLogs: false });

/**
 *  App configuration
 */
app.use(morgan('common'));
app.use(helmet());
app.use(cors());
app.use(express.json());

/**
 *  Routes configuration
 */
app.use('/api/v1/projects', ProjectsController);
app.use('/api/v1/trainings', TrainingsController);
app.use('/api/v1/experiences', ExperiencesController);

app.get('/', (req, res) => res.send(`Server listening at: http://localhost:${environment.API_PORT}`));

/**
 * Unknown routes handler definition
 */
app.all('*', UnknownRoutesHandler);

/**
 * Exceptions handler definition
 */
app.use(ExceptionsHandler);

/**
 * Database connection & Server activation
 */
connect(`mongodb://${process.env.MONGO_USER as string}:${process.env.MONGO_PWD as string}${environment.MONGO_URI}`).then(() => {
    Logger.info('Connected to MongoDB');
    app.listen(environment.API_PORT, () => Logger.info(`Server listening at: http://localhost:${environment.API_PORT}`));
}).catch(error => {
    Logger.error('Error connecting to DB: ', error);
    process.exit(1);
});