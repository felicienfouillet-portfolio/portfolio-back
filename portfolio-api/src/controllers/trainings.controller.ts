import { ApiSuccessResponse, AuthMiddlewares, BadRequestException, IApiResponse, NotFoundException } from '@felicienfouillet/dev-tools-back';
import { NextFunction, Request, Response, Router } from 'express';
import { TrainingsService } from 'services/trainings.service';

const TrainingsController = Router();

const service = new TrainingsService();
// const Logger: LoggerService = LoggerService.Instance({ loggerName: 'Portfolio API', saveLogs: false });

TrainingsController.get('/', async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const id = req.query.id as string;

        if (!id) {
            return res
                .status(200)
                .json(new ApiSuccessResponse(200, await service.findAll()));
        }

        const training = await service.findOne(id);

        if (!training) {
            throw new NotFoundException(new Error('No training found'));
        }

        return res
            .status(200)
            .json(new ApiSuccessResponse(200, training));
    } catch (error) {
        next(error)
    }
});

TrainingsController.post('/', AuthMiddlewares.verifyAccessToken, async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const createdTraining = await service.create(req.body);

        return res
            .status(201)
            .json(new ApiSuccessResponse(201, createdTraining));
    } catch (error) {
        next(error)
    }
});

TrainingsController.patch('/', AuthMiddlewares.verifyAccessToken, async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const id = req.query.id as string;

        if (!id) {
            throw new BadRequestException(new Error('Invalid id'));
        }

        const updatedTraining = await service.update(id, req.body);

        return res
            .status(200)
            .json(new ApiSuccessResponse(200, updatedTraining));
    } catch (error) {
        next(error)
    }
});

TrainingsController.delete('/', AuthMiddlewares.verifyAccessToken, async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const id = req.query.id as string;

        if (!id) {
            throw new BadRequestException(new Error('Invalid id'));
        }

        await service.delete(id);

        return res
            .status(200)
            .json(new ApiSuccessResponse(200, 'Project deleted'));
    } catch (error) {
        next(error)
    }
});

export { TrainingsController };

