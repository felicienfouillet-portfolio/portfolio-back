import { ApiSuccessResponse, AuthMiddlewares, BadRequestException, IApiResponse, NotFoundException } from '@felicienfouillet/dev-tools-back';
import { NextFunction, Request, Response, Router } from 'express';
import { ExperiencesService } from 'services/experiences.service';

const ExperiencesController = Router();

const service = new ExperiencesService();
// const Logger: LoggerService = LoggerService.Instance({ loggerName: 'Portfolio API', saveLogs: false });

ExperiencesController.get('/', async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const id = req.query.id as string;

        if (!id) {
            return res
                .status(200)
                .json(new ApiSuccessResponse(200, await service.findAll()));
        }

        const experience = await service.findOne(id);

        if (!experience) {
            throw new NotFoundException(new Error('No experience found'));
        }

        return res
            .status(200)
            .json(new ApiSuccessResponse(200, experience));
    } catch (error) {
        next(error)
    }
});

ExperiencesController.post('/', AuthMiddlewares.verifyAccessToken, async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const createdExperience = await service.create(req.body);

        return res
            .status(201)
            .json(new ApiSuccessResponse(201, createdExperience));
    } catch (error) {
        next(error)
    }
});

ExperiencesController.patch('/', AuthMiddlewares.verifyAccessToken, async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const id = req.query.id as string;

        if (!id) {
            throw new BadRequestException(new Error('Invalid id'));
        }

        const updatedExperience = await service.update(id, req.body);

        return res
            .status(200)
            .json(new ApiSuccessResponse(200, updatedExperience));
    } catch (error) {
        next(error)
    }
});

ExperiencesController.delete('/', AuthMiddlewares.verifyAccessToken, async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const id = req.query.id as string;

        if (!id) {
            throw new BadRequestException(new Error('Invalid id'));
        }

        await service.delete(id);

        return res
            .status(200)
            .json(new ApiSuccessResponse(200, 'Experience deleted'));
    } catch (error) {
        next(error)
    }
});

export { ExperiencesController };

