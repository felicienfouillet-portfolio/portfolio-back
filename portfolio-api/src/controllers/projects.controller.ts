import { ApiSuccessResponse, AuthMiddlewares, BadRequestException, IApiResponse, NotFoundException } from '@felicienfouillet/dev-tools-back';
import { NextFunction, Request, Response, Router } from 'express';
import { ProjectsService } from 'services/projects.service';

const ProjectsController = Router();

const service = new ProjectsService();
// const Logger: LoggerService = LoggerService.Instance({ loggerName: 'Portfolio API', saveLogs: false });

ProjectsController.get('/', async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const id = req.query.id as string;

        if (!id) {
            return res
                .status(200)
                .json(new ApiSuccessResponse(200, await service.findAll()));
        }

        const project = await service.findOne(id);

        if (!project) {
            throw new NotFoundException(new Error('No project found'));
        }

        return res
            .status(200)
            .json(new ApiSuccessResponse(200, project));
    } catch (error) {
        next(error)
    }
});

ProjectsController.post('/', AuthMiddlewares.verifyAccessToken, async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const createdProject = await service.create(req.body);

        return res
            .status(201)
            .json(new ApiSuccessResponse(201, createdProject));
    } catch (error) {
        next(error)
    }
});

ProjectsController.patch('/', AuthMiddlewares.verifyAccessToken, async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const id = req.query.id as string;

        if (!id) {
            throw new BadRequestException(new Error('Invalid id'));
        }

        const updatedProject = await service.update(id, req.body);

        return res
            .status(200)
            .json(new ApiSuccessResponse(200, updatedProject));
    } catch (error) {
        next(error)
    }
});

ProjectsController.delete('/', AuthMiddlewares.verifyAccessToken, async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    try {
        const id = req.query.id as string;

        if (!id) {
            throw new BadRequestException(new Error('Invalid id'));
        }

        await service.delete(id);

        return res
            .status(200)
            .json(new ApiSuccessResponse(200, 'Project deleted'));
    } catch (error) {
        next(error)
    }
});

export { ProjectsController };

