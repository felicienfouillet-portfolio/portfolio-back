import { LoggerService, NotFoundException } from '@felicienfouillet/dev-tools-back';
import { Experience, IExperience } from 'models/experiences.model';

export class ExperiencesService {
    private readonly Logger: LoggerService = LoggerService.Instance({ loggerName: 'Portfolio API', saveLogs: false });

    /**
     * Trouve tous les experiences
     */
    async findAll(): Promise<Array<IExperience>> {
        try {
            const experiences: Array<IExperience> = await Experience.find();

            return experiences;
        } catch (e) {
            throw new NotFoundException(new Error('No experiences found'));
        }
    }

    /**
     * Trouve un experience en particulier
     * @param id - ID unique de l'experience
     */
    async findOne(id: string): Promise<IExperience> {
        const experience: IExperience = await Experience.findById(id) as IExperience;

        if (!experience) {
            throw new NotFoundException(new Error('No experience found'));
        }

        return experience;
    }

    /**
     * Met à jour un experience en particulier
     *
     * /!\ Idéalement, il faudrait vérifier le contenu de la requête avant de le sauvegarder.
     *
     * @param experienceData - Un objet correspondant à un experience, il ne contient pas forcément tout un experience. Attention, on ne prend pas l'id avec.
     * @param id - ID unique de l'experience
     */
    async update(id: string, experienceData: Partial<IExperience>): Promise<IExperience> {
        const experience: IExperience = await this.findOne(id);

        if (!experience) {
            throw new NotFoundException(new Error('No experience found'));
        }

        const updatedExperience: IExperience = await Experience.findByIdAndUpdate(id, experienceData, { new: true }) as IExperience;

        return updatedExperience;
    }

    /**
     * Créé un experience
     *
     * /!\ Idéalement, il faudrait vérifier le contenu de la requête avant de le sauvegarder.
     *
     * @param experienceData - Un objet correspondant à un experience. Attention, on ne prend pas l'id avec.
     */
    async create(experienceData: IExperience): Promise<IExperience> {
        const newExperience: IExperience = await Experience.create(experienceData) as unknown as IExperience;

        return newExperience;
    }

    /**
     * Suppression d'un experience
     */
    async delete(id: string) {
        const experience = await this.findOne(id);

        if (!experience) {
            throw new NotFoundException(new Error('No experience found'));
        }

        await Experience.findByIdAndRemove(id);
    }
}
