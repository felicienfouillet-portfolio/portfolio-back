import { LoggerService, NotFoundException } from '@felicienfouillet/dev-tools-back';
import { ITraining, Training } from 'models/trainings.model';

export class TrainingsService {
    private readonly Logger: LoggerService = LoggerService.Instance({ loggerName: 'Portfolio API', saveLogs: false });

    /**
     * Trouve tous les trainings
     */
    async findAll(): Promise<Array<ITraining>> {
        try {
            const trainings: Array<ITraining> = await Training.find();

            return trainings;
        } catch (e) {
            throw new NotFoundException(new Error('No trainings found'));
        }
    }

    /**
     * Trouve un training en particulier
     * @param id - ID unique de l'training
     */
    async findOne(id: string): Promise<ITraining> {
        try {
            const training: ITraining = await Training.findById(id) as ITraining;

            return training;
        } catch (e) {
            throw new NotFoundException(new Error('No training found'));
        }
    }

    /**
     * Met à jour un training en particulier
     *
     * /!\ Idéalement, il faudrait vérifier le contenu de la requête avant de le sauvegarder.
     *
     * @param trainingData - Un objet correspondant à un training, il ne contient pas forcément tout un training. Attention, on ne prend pas l'id avec.
     * @param id - ID unique de l'training
     */
    async update(id: string, trainingData: Partial<ITraining>): Promise<ITraining> {
        const training: ITraining = await this.findOne(id);

        if (!training) {
            throw new NotFoundException(new Error('No training found'));
        }

        const updatedTraining: ITraining = await Training.findByIdAndUpdate(id, trainingData, { new: true }) as ITraining;

        return updatedTraining;
    }

    /**
     * Créé un training
     *
     * /!\ Idéalement, il faudrait vérifier le contenu de la requête avant de le sauvegarder.
     *
     * @param trainingData - Un objet correspondant à un training. Attention, on ne prend pas l'id avec.
     */
    async create(trainingData: ITraining): Promise<ITraining> {
        const newTraining: ITraining = await Training.create(trainingData) as unknown as ITraining;

        return newTraining;
    }

    /**
     * Suppression d'un training
     */
    async delete(id: string) {
        const training = await this.findOne(id);

        if (!training) {
            throw new NotFoundException(new Error('No training found'));
        }

        await Training.findByIdAndRemove(id);
    }
}
