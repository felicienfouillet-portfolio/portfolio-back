import { LoggerService, NotFoundException } from '@felicienfouillet/dev-tools-back';
import { IProject, Project } from 'models/projects.model';

export class ProjectsService {
    private readonly Logger: LoggerService = LoggerService.Instance({ loggerName: 'Portfolio API', saveLogs: false });

    /**
     * Trouve tous les projects
     */
    async findAll(): Promise<Array<IProject>> {
        try {
            const projects: Array<IProject> = await Project.find();

            return projects;
        } catch (e) {
            throw new NotFoundException(new Error('No projects found'));
        }
    }

    /**
     * Trouve un project en particulier
     * @param id - ID unique de l'project
     */
    async findOne(id: string): Promise<IProject> {
        try {
            const project: IProject = await Project.findById(id) as IProject;

            return project;
        } catch (e) {
            throw new NotFoundException(new Error('No project found'));
        }
    }

    /**
     * Met à jour un project en particulier
     *
     * /!\ Idéalement, il faudrait vérifier le contenu de la requête avant de le sauvegarder.
     *
     * @param projectData - Un objet correspondant à un project, il ne contient pas forcément tout un project. Attention, on ne prend pas l'id avec.
     * @param id - ID unique de l'project
     */
    async update(id: string, projectData: Partial<IProject>): Promise<IProject> {
        const project: IProject = await this.findOne(id);

        if (!project) {
            throw new NotFoundException(new Error('No project found'));
        }

        const updatedProject: IProject = await Project.findByIdAndUpdate(id, projectData, { new: true }) as IProject;

        return updatedProject;
    }

    /**
     * Créé un project
     *
     * /!\ Idéalement, il faudrait vérifier le contenu de la requête avant de le sauvegarder.
     *
     * @param projectData - Un objet correspondant à un project. Attention, on ne prend pas l'id avec.
     */
    async create(projectData: IProject): Promise<IProject> {
        const newProject: IProject = await Project.create(projectData) as unknown as IProject;

        return newProject;
    }

    /**
     * Suppression d'un project
     */
    async delete(id: string) {
        const project = await this.findOne(id);

        if (!project) {
            throw new NotFoundException(new Error('No project found'));
        }

        await Project.findByIdAndRemove(id);
    }
}
