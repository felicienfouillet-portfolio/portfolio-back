import { model, Schema } from 'mongoose';

export interface IExperience {
    _id: string;
    title: string;
    subtitle?: string;
    startDate?: string;
    endDate?: string;
    description?: string;
}

export const ExperienceSchema: Schema = new Schema({
    title: { type: String, required: true },
    subtitle: { type: String, required: false },
    startDate: { type: String, required: false },
    endDate: { type: String, required: false },
    description: { type: String, required: false }
});

export const Experience = model('Experience', ExperienceSchema);