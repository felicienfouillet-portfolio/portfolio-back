import { model, Schema } from 'mongoose';

export interface IProject {
    _id: string;
    title: string;
    subtitle?: string;
    imageUri: string;
    description?: string;
}

export const ProjectSchema: Schema = new Schema({
    title: { type: String, required: true },
    subtitle: { type: String, required: false },
    imageUri: { type: String, required: false },
    description: { type: String, required: false }
});

export const Project = model('Project', ProjectSchema);