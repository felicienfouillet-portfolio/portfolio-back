import { BadRequestException, LoggerService } from '@felicienfouillet/dev-tools-back';
import { NextFunction, Request, Response } from 'express';
import { UsersService } from 'services/users.service';

export abstract class UsersAuthMiddleware {
    private static Logger: LoggerService = LoggerService.Instance({ loggerName: 'Users API', saveLogs: false });

    /**
     * 
     * @param req 
     * @param res 
     * @param next 
     * @returns 
     */
    public static async verifyUserDucplication(req: Request, res: Response, next: NextFunction) {
        try {
            const mail: string = req.body.mail;

            if (!mail) {
                throw new BadRequestException(new Error('User mail not provided'));
            }

            const isUserDuplicated: boolean = await UsersService.isUserDuplicated(mail);

            if (isUserDuplicated) {
                throw new BadRequestException(new Error('User already exists'));
            }

            next();
        } catch (error) {
            UsersAuthMiddleware.Logger.error(error);
            next(error);
        }
    }
}