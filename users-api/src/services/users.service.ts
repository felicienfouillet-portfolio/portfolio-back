import { LoggerService, NotFoundException } from '@felicienfouillet/dev-tools-back';
import { IUser, Roles, User } from 'models/user.model';

export class UsersService {
    private readonly Logger: LoggerService = LoggerService.Instance({ loggerName: 'Users API', saveLogs: false });

    private static _instance: UsersService;
    public static get instance(): UsersService {
        return this._instance;
    }
    public static set instance(instance) {
        this._instance = instance;
    }

    constructor() {
        UsersService._instance = this;
    }

    /**
     * Trouve tous les users
     */
    async findAll(): Promise<Array<IUser>> {
        const users: Array<IUser> = await User.find();

        if (!users.length) {
            throw new NotFoundException(new Error('No users found'));
        }

        return users;
    }

    /**
     * Trouve un user en particulier
     * @param id - ID unique de l'user
     */
    async findOne(id: string): Promise<IUser> {
        const user: IUser = await User.findById(id) as IUser;

        if (!user) {
            throw new NotFoundException(new Error('No user found'));
        }

        return user;
    }

    /**
     * Trouve un utilisateur en particulier par son email
     * @param mail - mail unique de l'utilisateur
     */
    async findOneByMail(mail: string): Promise<IUser> {
        const user: IUser = await User.findOne({ mail }) as IUser;

        if (!user) {
            throw new NotFoundException(new Error('No user found'));
        }

        return user;
    }

    /**
     * Met à jour un user en particulier
     *
     * /!\ Idéalement, il faudrait vérifier le contenu de la requête avant de le sauvegarder.
     *
     * @param userData - Un objet correspondant à un user, il ne contient pas forcément tout un user. Attention, on ne prend pas l'id avec.
     * @param id - ID unique de l'user
     */
    async update(id: string, userData: Partial<IUser>): Promise<IUser> {
        const user: IUser = await this.findOne(id);

        if (!user) {
            throw new NotFoundException(new Error('No user found'));
        }

        if (userData.role && userData.role !== user.role) {
            userData.role = Roles.EXTERNAL;
        }

        const updatedUser: IUser = await User.findByIdAndUpdate(id, userData, { new: true }) as IUser;

        return updatedUser;
    }

    /**
     * Créé un user
     *
     * /!\ Idéalement, il faudrait vérifier le contenu de la requête avant de le sauvegarder.
     *
     * @param userData - Un objet correspondant à un user. Attention, on ne prend pas l'id avec.
     */
    async create(userData: IUser): Promise<IUser> {
        const newUser: IUser = await User.create(userData) as unknown as IUser;

        return newUser;
    }

    /**
     * Suppression d'un user
     */
    async delete(id: string) {
        const user = await this.findOne(id);

        if (!user) {
            throw new NotFoundException(new Error('No user found'));
        }

        await User.findByIdAndRemove(id);
    }

    /**
     * 
     * @param mail 
     * @returns 
     */
    public static async isUserDuplicated(mail: string): Promise<boolean> {
        const user = await this.instance.findOneByMail(mail);
        if (user === null) return false;
        return true;
    }
}
