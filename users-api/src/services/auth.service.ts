import { BadRequestException, getTokenPayload, LoggerService, NotFoundException, TokenData, UnauthorizedException } from '@felicienfouillet/dev-tools-back';
import * as bcrypt from 'bcrypt';
import jwt, { decode, JwtPayload, Secret, verify } from 'jsonwebtoken';
import { ILoginResponse, ITokenVerficationResponse } from 'models/auth.model';
import { IUser, Roles } from 'models/user.model';
import { UsersService } from './users.service';


export class AuthService {

    private readonly Logger: LoggerService = LoggerService.Instance({ loggerName: 'Auth API', saveLogs: false });

    /**
     * 
     * @param userInformationData 
     * @returns 
     */
    public async register(userInformationData: IUser): Promise<Partial<IUser>> {
        userInformationData.role = Roles.EXTERNAL;
        userInformationData.accessToken = '';
        userInformationData.refreshToken = '';
        userInformationData.isSuspended = false;
        const user = await UsersService.instance.create(userInformationData);

        this.Logger.info('New account registered');

        const userLight: Partial<IUser> = user;
        delete userLight.password;
        delete userLight.accessToken;
        delete userLight.isSuspended;

        return userLight;
    }

    /**
     * 
     * @param mail 
     * @param password 
     * @returns 
     */
    public async login(mail: string, password: string): Promise<ILoginResponse> {
        const user = await UsersService.instance.findOneByMail(mail);

        if (!user) throw new NotFoundException(new Error('No user found'));
        const hashComparaison = await bcrypt.compare(password, user.password);

        if (hashComparaison) {
            const tokenData: TokenData = {
                id: user._id,
                mail: user.mail,
                roleId: user.role,
                isSuspended: user.isSuspended,
            };

            const accessToken = this.generateAccessToken(tokenData);
            const refreshToken = this.generateRefreshToken(tokenData);

            const hashedAccessToken = await bcrypt.hash(accessToken.split('.')[2], 10);

            const updatedUser = user;
            updatedUser.accessToken = hashedAccessToken;
            updatedUser.refreshToken = refreshToken;

            await UsersService.instance.update(user._id, updatedUser);

            this.Logger.info('Account logged in');
            const userLight: Partial<IUser> = user;
            delete userLight.password;
            delete userLight.accessToken;
            delete userLight.isSuspended;

            const expires: number = (decode(accessToken) as JwtPayload).exp as number;

            return { user: userLight, accessToken: accessToken, refreshToken: refreshToken, expires: expires };
        }

        throw new UnauthorizedException(new Error('Unauthorized'));
    }

    /**
     * 
     * @param accessToken 
     * @returns 
     */
    public async verifyToken(accessToken: string): Promise<ITokenVerficationResponse> {
        let isVerifiedToken = false;

        const expires: number = (decode(accessToken) as JwtPayload).exp as number;
        if (expires > Date.now()) {
            return { isValid: false, reason: 'Session expire' };
        }

        try {
            const verified = verify(
                accessToken,
                process.env.ACCESS_TOKEN_SECRET as string
            );

            isVerifiedToken = verified ? true : false;
        } catch (error) {
            this.Logger.error(error);
        }

        if (!isVerifiedToken) {
            return { isValid: false, reason: 'Auth token invalid' };
        }

        const decodedToken = getTokenPayload(accessToken);

        const isSuspended: boolean = decodedToken?.isSuspended;
        const mail: string = decodedToken?.mail;

        if (isSuspended) {
            return { isValid: false, reason: 'Suspended profile' };
        }

        const user: Partial<IUser> = await UsersService.instance.findOneByMail(mail);

        const userLight: Partial<IUser> = user;
        delete userLight.password;
        delete userLight.accessToken;
        delete userLight.isSuspended;

        return {
            isValid: true,
            user: userLight,
            expires: expires
        };
    }

    /**
     * 
     * @param mail 
     * @returns 
     */
    public async logout(mail: string): Promise<void> {
        const user = await UsersService.instance.findOneByMail(mail);
        user.accessToken = '';
        user.refreshToken = '';

        await UsersService.instance.update(user._id, user);

        this.Logger.info('Account logged out');
    }

    /**
     * 
     * @param mail 
     * @param refreshToken 
     * @returns 
     */
    public async refreshToken(mail: string, accessToken: string, refreshToken: string): Promise<ILoginResponse> {
        const user = await UsersService.instance.findOneByMail(mail);

        const hashComparaison = await bcrypt.compare(accessToken ? accessToken.split('.')[2] : '', user.accessToken);
        if (!hashComparaison) throw new BadRequestException(new Error('Auth token Invalid'))
        if (user.refreshToken != refreshToken) throw new BadRequestException(new Error('Refresh Token Invalid'));

        const tokenData: TokenData = {
            id: user._id,
            mail: user.mail,
            roleId: user.role,
            isSuspended: user.isSuspended,
        };

        const newAccessToken = this.generateAccessToken(tokenData);
        const newRefreshToken = this.generateRefreshToken(tokenData);

        const hashedAccessToken = await bcrypt.hash(newAccessToken.split('.')[2], 10);

        const updatedUser = user;
        updatedUser.accessToken = hashedAccessToken;
        updatedUser.refreshToken = newRefreshToken;

        await UsersService.instance.update(user._id, updatedUser);

        const userLight: Partial<IUser> = user;
        delete userLight.password;
        delete userLight.accessToken;
        delete userLight.isSuspended;

        const expires: number = (decode(newAccessToken) as JwtPayload).exp as number;

        this.Logger.info('Account token refreshed');
        return {
            user: userLight,
            accessToken: newAccessToken,
            refreshToken: newRefreshToken,
            expires: expires
        };
    }

    /**
     * Génération d'un access token
     * 
     * @param tokenData 
     * @returns 
     */
    generateAccessToken(tokenData: TokenData): string {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        return jwt.sign(tokenData, process.env.ACCESS_TOKEN_SECRET as Secret, { expiresIn: '30m' });
    }

    /**
     * Génération d'un refresh token
     * 
     * @param tokenData 
     * @returns 
     */
    generateRefreshToken(tokenData: TokenData): string {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        return jwt.sign(tokenData, process.env.REFRESH_TOKEN_SECRET as Secret, { expiresIn: '60m' });
    }
}