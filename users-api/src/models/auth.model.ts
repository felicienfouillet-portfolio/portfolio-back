import { Tokens } from '@felicienfouillet/dev-tools-back';
import { IUser } from './user.model';

export interface ITokenVerficationResponse {
    isValid: boolean;
    reason?: string;
    user?: Partial<IUser>;
    expires?: number;
}

export interface ILoginResponse extends Tokens {
    user: Partial<IUser>;
    expires: number;
}