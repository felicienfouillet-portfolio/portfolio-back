import { model, Schema } from 'mongoose';

export interface IUser {
    _id: string;
    firstname: string;
    lastname: string;
    mail: string;
    password: string;
    role: Roles;

    refreshToken: string;
    accessToken: string;
    isSuspended: boolean;
}

export enum Roles {
    ADMIN = 0,
    EXTERNAL = 1
}


export const UserSchema: Schema = new Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    mail: { type: String, required: true },
    password: { type: String, required: true },
    role: { type: Number, required: true },

    refreshToken: { type: String, required: false },
    accessToken: { type: String, required: false },
    isSuspended: { type: Boolean, required: true }
});

export const User = model('User', UserSchema);