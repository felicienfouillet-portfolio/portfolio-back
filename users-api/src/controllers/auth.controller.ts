import { ApiErrorResponse, ApiSuccessResponse, IApiResponse, LoggerService, Tokens } from '@felicienfouillet/dev-tools-back';
import * as bcrypt from 'bcrypt';
import { NextFunction, Request, Response, Router } from 'express';
import { IUser } from 'models/user.model';
import { AuthService } from 'services/auth.service';

const Logger: LoggerService = LoggerService.Instance({ loggerName: 'Auth API', saveLogs: false });

/**
 * Nous créons un `Router` Express, il nous permet de créer des routes en dehors du fichier `src/index.ts`
 */
const AuthController = Router();

/**
 * Instance de notre service
 */
const authService = new AuthService();

/**
 * Enregistrer un nouvel utilisateur
 */
AuthController.post('/register', async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    Logger.info('Requesting account registration');
    try {
        const hashedPassword = await bcrypt.hash(req.body.password, 10);

        const userInformationData: any = {
            ...req.body,
            password: hashedPassword
        }

        const createdUser: Partial<IUser> = await authService.register(userInformationData);

        return res
            .status(201)
            .json(new ApiSuccessResponse(201, createdUser));
    } catch (error: any) {
        Logger.error(error);
        next(error);
    }
});


/**
 * Connecter un utilisateur
 */
AuthController.post('/login', async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    Logger.info('Requesting account login');
    try {
        const loginInformations = await authService.login(req.body.mail, req.body.password);
        return res.status(201).json(new ApiSuccessResponse(201, loginInformations));
    } catch (error: any) {
        Logger.error(error);
        next(error);
    }
});

/**
 * Verifier un token
 */
AuthController.get('/verifyToken', async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    Logger.info('Requesting token verification');
    try {
        const accessToken: string = req.headers['x-access-token'] as string;

        if (!accessToken) {
            return res.status(400).send(new ApiErrorResponse(400, 'Auth token not provided'));
        }

        const tokenVerification = await authService.verifyToken(accessToken);
        return res.status(201).json(new ApiSuccessResponse(201, tokenVerification));
    } catch (error: any) {
        Logger.error(error);
        next(error);
    }
});

/**
 * Rafraichir le token d'accès d'un utilisateur
 */
AuthController.post('/refreshToken', async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    Logger.info('Requesting account token refresh');
    try {
        const accessToken: string = req.headers['x-access-token'] as string;
        const tokens: Tokens = await authService.refreshToken(req.body.mail, accessToken, req.body.refreshToken) as Tokens;

        return res.status(200).send(new ApiSuccessResponse(201, tokens));
    } catch (error: any) {
        Logger.error(error);
        next(error);
    }
});

/**
 * Déconnecter un utilisateur
 */
AuthController.post('/logout', async (req: Request, res: Response<IApiResponse>, next: NextFunction) => {
    Logger.info('Requesting account logout');
    try {
        await authService.logout(req.body.mail);

        return res.status(204).send(new ApiSuccessResponse(201, 'Disconnected'));
    } catch (error: any) {
        Logger.error(error);
        next(error);
    }
});

/**
 * On expose notre controller pour l'utiliser dans `src/index.ts`
 */
export { AuthController };

