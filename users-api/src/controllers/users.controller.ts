import { AuthMiddlewares, BadRequestException, LoggerService } from '@felicienfouillet/dev-tools-back';
import * as bcrypt from 'bcrypt';
import { Router } from 'express';
import { UsersService } from 'services/users.service';

const Logger: LoggerService = LoggerService.Instance({ loggerName: 'Users API', saveLogs: false });

/**
 * Nous créons un `Router` Express, il nous permet de créer des routes en dehors du fichier `src/index.ts`
 */
const UsersController = Router();

/**
 * Instance de notre usersService
 */
const usersService = new UsersService();

/**
 * Trouve tous les utilisateurs
 */
UsersController.get('/', AuthMiddlewares.hasAdminRole, async (req, res, next) => {
    Logger.info('Requesting all users');
    try {
        return res
            .status(200)
            .json(await usersService.findAll());
    } catch (error) {
        Logger.error(error);
        next(error);
    }
});

/**
 * Trouve un utilisateur en particulier par son email ou son id
 */
UsersController.get('/:id', AuthMiddlewares.hasAdminRole, AuthMiddlewares.verifyProfileOwnership, async (req, res, next) => {
    Logger.info('Requesting single user');
    try {
        const id = req.params.id;

        if (!id) {
            throw new BadRequestException(new Error('Invalid parameters'));
        }

        const user = await usersService.findOne(id);

        return res
            .status(200)
            .json(user);

    } catch (error) {
        Logger.error(error);
        next(error);
    }
});

/**
 * Créer un utilisateur
 */
// UsersController.post('/', UsersAuthMiddleware.verifyUserDucplication, async (req, res, next) => {
//     Logger.info('Requesting user creation');
//     try {
//         const createdUser = await usersService.create(req.body);

//         return res
//             .status(201)
//             .json(createdUser);
//     } catch (error) {
//         Logger.error(error);
//         next(error);
//     }
// });


/**
 * Mettre à jour le mot de passe d'un utilisateur
 */
UsersController.patch('/updatePassword/:id', AuthMiddlewares.verifyProfileOwnership, async (req, res, next) => {
    Logger.info('Requesting account password update');
    try {
        const id = req.params.id;

        if (!id) {
            throw new BadRequestException(new Error('Invalid id'));
        }

        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        console.log(hashedPassword);

        const userInformationData: any = {
            password: hashedPassword
        }

        const updatedUser = await usersService.update(id, userInformationData);

        return res.status(200).send(updatedUser);
    } catch (error: any) {
        Logger.error(error);
        next(error);
    }
});

/**
 * Mise à jour d'un utilisateur
 */
UsersController.patch('/:id', AuthMiddlewares.verifyProfileOwnership, async (req, res, next) => {
    Logger.info('Requesting user update');
    try {
        const id = req.params.id;

        if (!id) {
            throw new BadRequestException(new Error('Invalid id'));
        }

        const updatedUser = await usersService.update(id, req.body);

        return res
            .status(200)
            .json(updatedUser);
    } catch (error) {
        Logger.error(error);
        next(error);
    }
});

/**
 * Suppression d'un utilisateur
 */
UsersController.delete('/:id', AuthMiddlewares.hasAdminRole, AuthMiddlewares.verifyProfileOwnership, async (req, res, next) => {
    Logger.info('Requesting user deletion');
    try {
        const id = req.params.id;

        if (!id) {
            throw new BadRequestException(new Error('Invalid id'));
        }

        const response = await usersService.delete(id);

        return res
            .status(200)
            .json(response);
    } catch (error) {
        Logger.error(error);
        next(error);
    }
});

/**
 * On expose notre controller pour l'utiliser dans `src/index.ts`
 */
export { UsersController, usersService };

